{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.python3
    pkgs.python3Packages.pyaml
  ];

  shellHook = ''
    echo "Environment prepared with Flask and feedparser"
  '';
}
