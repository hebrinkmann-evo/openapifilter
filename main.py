import yaml
import argparse

def load_openapi_spec(file_path):
    """
    Load and return the OpenAPI specification from a YAML file.
    """
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)

def read_included_paths(file_path):
    """
    Read and return the list of included paths from a file.
    """
    with open(file_path, 'r') as file:
        return [line.strip() for line in file.readlines()]

def find_referenced_components(schema, openapi_spec, found_components):
    """
    Recursively find all components referenced in a given schema.
    """
    if '$ref' in schema:
        ref = schema['$ref']
        if ref.startswith('#/components/schemas/'):
            ref_name = ref.split('/')[-1]
            if ref_name not in found_components:
                found_components.add(ref_name)
                new_schema = openapi_spec['components']['schemas'][ref_name]
                find_referenced_components(new_schema, openapi_spec, found_components)

    for key, value in schema.items():
        if isinstance(value, dict):
            find_referenced_components(value, openapi_spec, found_components)
        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    find_referenced_components(item, openapi_spec, found_components)

def find_used_components(openapi_spec, included_paths):
    """
    Find all components used by the included paths.
    """
    used_components = set()

    for path, methods in openapi_spec['paths'].items():
        if path not in included_paths:
            continue

        for operation in methods.values():
            for response in operation.get('responses', {}).values():
                content = response.get('content', {})
                for media_type in content.values():
                    schema = media_type.get('schema', {})
                    find_referenced_components(schema, openapi_spec, used_components)

    return used_components

def filter_openapi_spec(openapi_spec, included_paths):
    """
    Filter the OpenAPI specification to only include specified paths and their used components.
    """
    # Filter paths
    filtered_paths = {path: details for path, details in openapi_spec['paths'].items() if path in included_paths}
    openapi_spec['paths'] = filtered_paths

    # Find and filter components
    used_components = find_used_components(openapi_spec, included_paths)
    filtered_components = {comp: openapi_spec['components']['schemas'][comp] for comp in used_components}
    openapi_spec['components']['schemas'] = filtered_components

    return openapi_spec

def save_openapi_spec(openapi_spec, output_file_path):
    """
    Save the OpenAPI specification to a YAML file.
    """
    with open(output_file_path, 'w') as file:
        yaml.dump(openapi_spec, file, default_flow_style=False)

def main():
    parser = argparse.ArgumentParser(description='Filter an OpenAPI specification.')
    parser.add_argument('--input', required=True, help='Path to the input OpenAPI YAML file.')
    parser.add_argument('--output', required=True, help='Path to the output OpenAPI YAML file.')
    parser.add_argument('--paths', required=True, help='File containing the list of included paths, one per line.')
    args = parser.parse_args()

    openapi_spec = load_openapi_spec(args.input)
    included_paths = read_included_paths(args.paths)
    filtered_spec = filter_openapi_spec(openapi_spec, included_paths)
    save_openapi_spec(filtered_spec, args.output)

    print(f"Filtered OpenAPI specification has been saved to {args.output}")

if __name__ == "__main__":
    main()
